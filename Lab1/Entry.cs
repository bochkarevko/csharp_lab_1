﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
  class Entry
  {
    static uint count = 0;
    uint id_;
    public uint ID { get { return id_; } }
    //Obligatory fields
    string name_;
    string surname_;
    ulong number_;
    string country_;
    //Optional fields
    string fname_;
    string birthday_;
    string company_;
    string position_;
    List<string> notes_ = new List<string>();

    public Entry()
    {
      count++;
      id_ = count;
      Console.Write("\nEnter the name: ");
      ReadObligatory(out this.name_);
      Console.Write("Enter the surname: ");
      ReadObligatory(out this.surname_);
      Console.Write("Enter father's name (can be left empty): ");
      ReadOptional(out this.fname_);
      Console.Write("Enter phone number: ");
      GetNumber(out this.number_);
      Console.Write("Enter the country: ");
      ReadObligatory(out this.country_);
      Console.Write("Enter birthday (can be left empty): ");
      ReadOptional(out this.birthday_);
      Console.Write("Enter company name (can be left empty): ");
      ReadOptional(out this.company_);
      Console.Write("Enter position (can be left empty): ");
      ReadOptional(out this.position_);
      for(int i = 0; ; ++i)
      {
        Console.Write($"Enter note {i + 1} (can be left empty): ");
        ReadOptional(out string tmp);
        if(tmp == "[field left empty]")
        {
          break;
        }
        else
        {
          notes_.Add(tmp);
        }

      }
      Console.WriteLine($"Entry {id_} created.\n");
    }

    public Entry(Entry old_entry)
    {
      this.name_ = old_entry.name_;
      this.surname_ = old_entry.surname_;
      this.number_ = old_entry.number_;
      this.country_ = old_entry.country_;
      this.fname_ = old_entry.fname_;
      this.birthday_ = old_entry.birthday_;
      this.company_ = old_entry.company_;
      this.position_ = old_entry.position_;
      this.notes_ = new List<string>(old_entry.notes_); //this does not make a copy of the list probably
    }

    static void ReadObligatory(out string field)
    {
      field = Console.ReadLine();
      while(field == "")
      {
        Console.Write("This field cannot be left empty: ");
        field = Console.ReadLine();
      }
    }

    static void ReadOptional(out string field)
    {
      field = Console.ReadLine();
      if(field == "")
      {
        field = "[field left empty]";
      }
    }

    static void GetNumber(out ulong number)
    {
      bool not_done = true;
      string user_input = Console.ReadLine();
      ;

      while(not_done)
      {
        if(user_input == "")
        {
          Console.Write("This field cannot be left empty: ");
          user_input = Console.ReadLine();
          continue;
        }
        try
        {
          number = Convert.ToUInt64(user_input);
          not_done = false;
        }
        catch(System.OverflowException)
        {
          Console.Write($"Enter a valid number using only digits: ");
          user_input = Console.ReadLine();
        }
        catch(System.FormatException)
        {
          Console.Write($"Enter a valid number using only digits: ");
          user_input = Console.ReadLine();
        }
      }
      number = Convert.ToUInt64(user_input);
    }

    public static void EditEntry(List<Entry> entries, string input_id)
    {
      uint entry_id;
      try
      {
        entry_id = Convert.ToUInt32(input_id);
      }
      catch(System.OverflowException)
      {
        Console.WriteLine($"\nEntry {input_id} does not exist.");
        return;
      }
      catch(System.FormatException)
      {
        Console.WriteLine($"\nEntry {input_id} does not exist.");
        return;
      }
      Entry old_entry = entries.Find(x => x.ID == entry_id);
      if(old_entry == null)
      {
        Console.WriteLine($"\nEntry {entry_id} does not exist.");
        return;
      }

      Entry new_entry = new Entry(old_entry);
      string user_input, new_info;

      const string info = "\nEnter 'exit' to stop editing. Enter 'help' or 'help-verbose' for help.";
      const string help = "\nAvailable commands:\n\thelp | help-verbose | exit | current | fields | [field name] [edit]";
      const string fields = "\nname | surname | fname (Father's name) | number | country | birthdate | company | position | notes";
      const string help_verbose = "\nEnter:\n\t'help' to get the list of all available commands;\n\t'help--verbose' to get the list of all available commands with their description;\n\t" +
      "'ext' yo stop editing;\n\t'current' to see the current state of the entry;'fields' to se all available fields;\n\t'notes to edit notes;\n\t'[field name] [edit]' to edit a field;";

      Console.WriteLine($"\nTo edit this entry enter '[field name] [edit]'.{new_entry}{info}");
      do
      {
        Program.GetUserInput(out user_input, out new_info);
        switch(user_input)
        {
          //fields
          case "name":
            EditMandatoryField(new_info, user_input, ref new_entry.name_);
            break;
          case "fname":
            EditOptionalField(new_info, ref new_entry.fname_);
            break;
          case "surname":
            EditMandatoryField(new_info, user_input, ref new_entry.surname_);
            break;
          case "country":
            EditMandatoryField(new_info, user_input, ref new_entry.country_);
            break;
          case "company":
            EditOptionalField(new_info, ref new_entry.company_);
            break;
          case "position":
            EditOptionalField(new_info, ref new_entry.position_);
            break;
          case "number":
            EditNumber(new_info, ref new_entry.number_);
            break;
          case "birthday":
            EditOptionalField(new_info, ref new_entry.birthday_);
            break;
          case "notes":
            new_entry.EditNotes();
            break;
          //other commands
          case "exit":
            Console.WriteLine("\nLeaving entry editing...");
            break;
          case "current":
            Console.WriteLine(new_entry);
            break;
          case "fields":
            Console.WriteLine(fields);
            break;
          case "help":
            Console.WriteLine(help);
            break;
          case "help-verbose":
            Console.WriteLine(help_verbose);
            break;
          default:
            Console.WriteLine($"\nCommand not recognized: {user_input}.{info}");
            break;
        }
      } while(user_input != "exit");

      Console.WriteLine($"\nOld entry:\n{old_entry}\nEdited entry:\n{new_entry}\nDo you wish to save changes? 'yes' to save, anything else to discard changes.");
      user_input = Console.ReadLine().ToLower();

      if(user_input == "yes")
      {
        new_entry.id_ = old_entry.id_;
        entries.RemoveAll(x => x.id_ == entry_id);
        entries.Add(new_entry);
        Console.WriteLine("\nChanges saved.");
      }
      else
      {
        Console.WriteLine("\nChanges discarded.");
      }
    }

    static void EditMandatoryField(string new_info, string field_name, ref string field)
    {
      if(new_info == "")
      {
        Console.WriteLine("This field can't be empty. Edit discarded.");
      }
      else
      {
        field = new_info;
        Console.WriteLine($"Field {field_name} edited.");
      }

    }

    static void EditOptionalField(string new_info, ref string field)
    {
      field = new_info;
      if(field == "")
      {
        field = "[field left empty]";
      }
    }

    static void EditNumber(string new_info, ref ulong number)
    {
      try
      {
        number = Convert.ToUInt64(new_info);
        ;
      }
      catch(System.OverflowException)
      {
        Console.Write($"\nOnly digits allowed.");
      }
      catch(System.FormatException)
      {
        Console.Write($"\nOnly digits allowed.");
      }
    }

    public void EditNotes()
    {
      string user_command, user_input;

      const string info = "\nEnter 'help' or 'help-verbose' for help, 'exit' to stop editing.";
      const string help = "\nAvailable commands:\n\thelp | help-verbose | exit | [note number] [edit] | notes";
      const string help_verbose = "\nEnter:\n\t'[note number] [edit]' to edit a note;\n\t'new [new information]' to create a new note;" +
          "\n\t'remove [note number]' to remove a note;\n\t'notes' to see all notes in the entry;" +
          "\n\t!WARNING! When editing notes, their numbers change. Be cautious.";

      Console.WriteLine("\nEditing notes." + info);

      do
      {
        Program.GetUserInput(out user_command, out user_input);
        switch(user_command)
        {
          case "help":
            Console.WriteLine(help);
            break;
          case "help-verbose":
            Console.WriteLine(help_verbose);
            break;
          case "exit":
            Console.WriteLine("\nLeaving notes editing...");
            break;
          case "notes":
            Console.WriteLine("\nThis entry notes:");
            for(int i = 0; i < notes_.Count; ++i)
            {
              Console.WriteLine($"\tNote {i + 1}: {notes_[i]};");
            }
            break;
          case "remove":
            RemoveANote(user_input, info);
            break;
          case "new":
            notes_.Add(user_input);
            break;
          default:
            EditANote(user_command, user_input, info);
            break;
        }
      } while(user_command != "exit");
    }

    void EditANote(string user_command, string new_note, string info)
    {
      int note_number;
      try
      {
        note_number = Int32.Parse(user_command) - 1;
        if(note_number < 0 || note_number > notes_.Count - 1)
        {
          Console.WriteLine($"\nNote {user_command} does not exist.{info}");
          return;
        }
        else
        {
          notes_[note_number] = new_note;
          Console.WriteLine($"\nNote {user_command} edited.{info}");
        }
      }
      catch(FormatException)
      {
        Console.WriteLine($"\nCommand not recognized '{user_command}'{info}");
      }
    }

    void RemoveANote(string user_input, string info)
    {
      int note_number;
      try
      {
        note_number = Int32.Parse(user_input) - 1;
        if(note_number < 0 || note_number > notes_.Count - 1)
        {
          Console.WriteLine($"\nNote {user_input} does not exist.{info}");
          return;
        }
        else
        {
          if(Program.Confirm())
          {
            notes_.RemoveAt(note_number);
            Console.WriteLine($"\nNote {user_input} removed.{info}");
          }
        }
      }
      catch(FormatException)
      {
        Console.WriteLine($"\nWrong format, should be a number: '{user_input}'{info}");
      }
    }

    public void ShortShow()
    {
      Console.WriteLine($"\tID: {id_} | Surname: {surname_} | Name: {name_} | Phone number: {number_};");
    }

    public override string ToString()
    {
      string full = $"\n\tID: {id_} | Surname: {surname_} | Name: {name_} | Father's name: {fname_}  | Phone number: {number_}\n" +
          $"\tCountry: {country_} | Birthdate: {birthday_} | Company: {company_} | Position: {position_}\n";
      for(int i = 0; i < notes_.Count; ++i)
      {
        full += $"\tNote {i + 1}: {notes_[i]}\n";
      }
      return full;
    }
  }
} //namespace Lab1
