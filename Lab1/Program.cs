﻿using System;
using System.Collections.Generic;

namespace Lab1
{
  class Program
  {
    const string info = "\nEnter 'exit' to exit the program. Enter 'help' or 'help-verbose' for help.";
    const string help = "\nAvailable commands:\n\thelp | help-verbose | exit | add | remove [id] | edit [id] | all | all-verbose | check [id]";
    const string help_verbose = "\nEnter:\n\t'help' to get the list of all available commands;\n\t'help--verbose' to get the list of all available commands with their description;\n\t" +
        "'add' to add a new entry;\n\t'remove [id]' to delete the entry with the given id;\n\t'edit [id]' to edit the entry with the given id;\n\t" +
        "'check [id]' to get full entry with the given id;\n\t'all' to see the list of all existing entries;\n\t'all-verbose' to see all entries in full\n\t" +
        "'exit' to stop this session;";
    List<Entry> entries = new List<Entry>();

    static void Main(string[] args)
    {
      Program PhoneBook = new Program();
      string user_command, user_input;

      Console.WriteLine("Welcome to \"The Lab 1\" phone-book." + info);
      //main loop
      do
      {
        GetUserInput(out user_command, out user_input);
        switch(user_command)
        {
          case "add":
            PhoneBook.entries.Add(new Entry());
            break;
          case "remove":
            PhoneBook.RemoveEntry(user_input);
            break;
          case "all":
            PhoneBook.ShowAllEntries(false);
            break;
          case "all-verbose":
            PhoneBook.ShowAllEntries(true);
            break;
          case "check":
            PhoneBook.ShowEntry(user_input);
            break;
          case "help":
            Console.WriteLine(help);
            break;
          case "help-verbose":
            Console.WriteLine(help_verbose);
            break;
          case "edit":
            Entry.EditEntry(PhoneBook.entries, user_input);
            break;
          case "exit":
            PhoneBook.Exit(out user_command);
            break;
          default:
            Console.WriteLine($"\nSorry, command {user_command} not recognized." + info);
            break;
        }
      } while(user_command != "exit");
    }

    void RemoveEntry(string input_id)
    {
      uint entry_id;
      try
      {
        entry_id = Convert.ToUInt32(input_id);
      }
      catch(System.OverflowException)
      {
        Console.WriteLine($"\nEntry {input_id} does not exist.");
        return;
      }
      catch(System.FormatException)
      {
        Console.WriteLine($"\nEntry {input_id} does not exist.");
        return;
      }
      if(entries.Exists(x => x.ID == entry_id))
      {
        if(Program.Confirm())
        {
          entries.RemoveAll(x => x.ID == entry_id);
          Console.WriteLine($"\nEntry {entry_id} removed.");
        }
      }
      else
      {
        Console.WriteLine($"\nEntry {entry_id} does not exist.");
      }
    }

    void ShowAllEntries(bool full)
    {
      if(full)
      {
        Console.WriteLine("\nExisting entries:");
        foreach(Entry entry in entries)
        {
          Console.WriteLine(entry);
        }
      }
      else
      {
        Console.WriteLine("\nList of all entries:");
        foreach(Entry entry in entries)
        {
          entry.ShortShow();
        }
      }
    }

    void ShowEntry(string user_input)
    {
      uint entry_id = Convert.ToUInt32(user_input);
      Console.WriteLine(entries.Find(x => x.ID == entry_id));
    }

    void Exit(out string user_input)
    {
      Console.WriteLine("All the entries will be lost, since the program does not support saving to a file yet. \n" +
          "Are you sure you want to exit the program? Enter 'exit' to exit or anything else to stay.");
      user_input = Console.ReadLine().ToLower();
      if(user_input == "exit")
      {
        Console.WriteLine("Thanks for using \"The Lab 1\" phone-book. Have a nice day!\n" +
            "(Press any key to close the window.)");
        Console.ReadKey();
      }
    }

    public static void GetUserInput(out string user_command, out string argument)
    {
      string raw_user_input = Console.ReadLine();
      user_command = raw_user_input.Split(' ')[0].ToLower();
      try
      {
        argument = raw_user_input.Substring(user_command.Length + 1);
      }
      catch(System.ArgumentOutOfRangeException)
      {
        argument = "";
      }
    }

    public static bool Confirm()
    {
      Console.WriteLine("\nEnter 'yes' to confirm action. Enter anything else to cancel.");
      string user_input = Console.ReadLine().ToLower();
      if(user_input == "yes")
      {
        return true;
      }
      else
      {
        Console.WriteLine("\nAction cancelled.");
        return false;
      }
    }
  }
} //namespace Lab1
